const imageInput = document.querySelector(".item-image-input");
const imageInputWrapper = document.querySelector(".item-image-wrapper");
const itemsContainer = document.querySelector(".items-container");
const itemTitleInput = document.querySelector(".item-title");
const itemDescriptionInput = document.querySelector(".item-description");
const addItemForm = document.querySelector(".add-item-form");

const places = JSON.parse(localStorage.getItem("places")) || [
  {
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
    image: "./assets/images/card1-image.png",
    name: "Pão de Açúcar",
  },
  {
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
    image: "./assets/images/card2-image.png",
    name: "Ilha Grande",
  },
  {
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
    image: "./assets/images/card3-image.png",
    name: "Cristo Redentor",
  },
  {
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
    image: "./assets/images/card4-image.png",
    name: "Centro Histórico de Paraty",
  },
];

window.addEventListener("load", renderItems(places));
imageInput.addEventListener("change", displayImagePreview);
addItemForm.addEventListener("submit", (e) => {
  addItem(e);
});

function displayImagePreview() {
  let image = imageInput.files[0];

  let fileReader = new FileReader();
  fileReader.readAsDataURL(image);
  fileReader.onload = () => {
    let imagePreview = document.createElement("img");
    imagePreview.setAttribute("src", fileReader.result);
    imagePreview.classList.add("item-image-preview");
    imageInputWrapper.appendChild(imagePreview);
  };
}

function renderItems(arr) {
  arr.forEach((elem) => {
    let item = createItem(elem);

    itemsContainer.appendChild(item);
  });
}

function createItem(elem) {
  let item = document.createElement("div");
  item.classList.add("item-card");

  item.innerHTML = `
      <img
      src=${elem.image}
      alt="item-image"
      class="item-card-image"
    />
    <div class="item-card-info">
      <h3 class="item-card-title">${elem.name}</h3>

      <p class="item-card-description">
        ${elem.description}
      </p>
    </div>
  `;
  return item;
}

function addItem(e) {
  e.preventDefault();
  const image = imageInput.files[0];
  const name = itemTitleInput.value;
  const description = itemDescriptionInput.value;

  let fileReader = new FileReader();
  fileReader.readAsDataURL(image);

  fileReader.onload = () => {
    const item = {
      image: fileReader.result,
      name: name,
      description: description,
    };
    places.push(item);

    let itemDOM = createItem(places[places.length - 1]);
    itemsContainer.appendChild(itemDOM);

    reset();

    localStorage.setItem("places", JSON.stringify(places));
  };
}

function reset() {
  const imagePreview = document.querySelector(".item-image-preview");
  itemTitleInput.value = "";
  itemDescriptionInput.value = "";
  imageInputWrapper.removeChild(imagePreview);
}
